new Vue({
	el: '#desafio',

	data: {
		estilo: 'destaque',
		perigoProp: true,
		estilo3: '',
		estilo4: '',
		cor5: '',
		estilo5: {
			width: '100px',
			height: '100px'
		},
		porcentagem: '',
		estilo6: ''
	},

	methods: {
		iniciarEfeito() {
			setInterval(() => {
				this.estilo = this.estilo == 'destaque' ? 'encolher' : 'destaque'
			}, 1000)
		},

		iniciarProgresso() {
			let valor = 0;
			const temporizador = setInterval(() => {
				this.estilo6 = {background: "green"};
				valor += 5;
				this.porcentagem = `${valor}%`;

				if(valor == 100) clearInterval(temporizador)
			}, 500)
		},

		setPerigo(event) {
			if (event.target.value == "true") {
				this.perigoProp = true;
			} else if (event.target.value == "false") {
				this.perigoProp = false;
			}
		}
	}
})
