new Vue({
    el: '#app',

    data() {
        return {
            running: false,
            playerLife: 100,
            monsterLife: 100,
            logs: []
        }
    },

    computed: {
        hasResult() {
            return this.playerLife == 0 || this.monsterLife == 0
        }
    },

    methods: {
        startGame() {
            this.running = true
            this.playerLife = 100
            this.monsterLife = 100
            this.registerLog('Iniciando jogo...', 'new')
        },

        endGame() {
            this.running = false
            this.playerLife = 100
            this.monsterLife = 100
            this.logs = []
        },

        attack(especial) {
            this.hurt('playerLife', 7, 12, false, 'Jogador', 'Monstro', 'player')

            if(this.monsterLife > 0) {
                this.hurt('monsterLife', 5, 10, especial, 'Monstro', 'Jogador', 'monster')
            }
        },

        hurt(prop, min, max, especial, source, target, cls) {
            const plus = especial ? 5 : 0;
            const hurt = this.getRandom(min + plus, max + plus)
            this[prop] = Math.max(this[prop] - hurt, 0) // Uso Math.max para garantir que o playerLife nunca será negativo. Se a diferença do (playerLife - hurt) for negativo, o maior número entre os dois será 0, se não pego o valor normal do cálculo 
            this.registerLog(`${source} atingiu ${target} com ${hurt}.`, cls)
        },

        /* Gero um valor entre 0 e 1 e multiplico com o valor de (10 - 5) e desloco pelo valor mínimo e aí tenho quase um intervalo entre 5 e 10, 
        mas o fato que 10 nunca irá entrar e depois faço o arredondamento que tanto pode ir para 5 ou 10  */
        getRandom(min, max) {
            const value = Math.random() * (max-min) + min
            return Math.round(value)
        },

        heal(min, max) {
            const heal = this.getRandom(min, max)
            this.playerLife = Math.min(this.playerLife + heal, 100) // Mesma ideia do Math.max porem agora o mínimo pego 100
            this.registerLog(`Jogador ganhou força de ${heal}.`, 'player')
        },

        healAndHurt() {
            this.heal(10, 15)
            this.hurt('playerLife', 7, 12, false, 'Monstro', 'Jogador', 'monster')
        },

        registerLog(text, cls) {
            this.logs.unshift({text, cls}) // unshift significa que vou colocar o elemento na primeira posição do array, então na lista sempre será exibido o log mais recente no começo e os antigos vão descendo
        }

    },

    watch: { // watch serve para monitorar a alteração de uma propriedade data e computed
        hasResult(value) {
            if(value) this.running = false
        }
    },
})